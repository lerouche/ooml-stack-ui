(function(undefined) {
	"use strict";

	var dom = document.createElement('div');
	dom.innerHTML =
		'<template ooml-abstract-class="Layer">' +
			'<ooml-property name="layerID">null</ooml-property>' +

			'<ooml-method name="constructor">' +
				'function() {' +
					'this.layerID = StackUI.layer.create({ $layer: this.$layer });' +
				'}' +
			'</ooml-method>' +

			'<ooml-method name="open">' +
				'function() {' +
					'StackUI.layer.open(this.layerID);' +
				'}' +
			'</ooml-method>' +

			'<ooml-method name="close">' +
				'function() {' +
					'StackUI.layer.close(this.layerID);' +
				'}' +
			'</ooml-method>' +

			'<ooml-method name="destroy">' +
				'function() {' +
					'StackUI.layer.destroy(this.layerID);' +
				'}' +
			'</ooml-method>' +
		'</template>';

	var ooml = new OOML.Namespace(dom);

	StackUI.OOML = {
		Layer: ooml.classes.Layer
	};
})();