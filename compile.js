const fs = require('fs');
const SRC_DIR = __dirname + '/src';

var args = process.argv.slice(2);
var dest = args.find(arg => /^\-o=.+$/.test(arg));

dest = dest ? dest.slice(3) : __dirname + '/dist';

require('zcompile')({
	source: SRC_DIR,
	destination: dest,

	files: fs.readdirSync(SRC_DIR),
});